json.extract! location, :id, :name, :group_id, :product_id, :created_at, :updated_at
json.url location_url(location, format: :json)
