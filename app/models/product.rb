class Product < ApplicationRecord
  has_many :locations
  has_many :groups, through: :locations
end
