class Group < ApplicationRecord
  has_many :locations
  has_many :products, through: :locations
end
