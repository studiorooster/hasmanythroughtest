Rails.application.routes.draw do

  resources :locations do
    resources :groups do
      resources :products
    end
  end

  root :to => 'locations#index'
end
